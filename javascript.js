// bai 1:Tìm số nguyên dương nhỏ nhất
// input: giá trị giới hạn (10000)
// action:
// cho vòng lập thực hiện khi tổng nhỏ hơn giá trị giới hạn, sau đó in ra số lần lập
// output: số lần lập
function handleClick(){
    const result=document.getElementById("result")
    let sum=0
    let i=0
    while(sum<10000){
        ++i
        sum=sum+i
    }
    result.innerHTML=i-1
}

// bai 2:Viết chương trình nhập vào 2 số x, n
// input: 2 số x và n;
// action:
// tính số mũ bằng hàm Math.pow sau đó tính tổng của chúng
// cho vòng lập thực hiện khi giá trị i nhỏ hơn số mũ sau đó in ra tổng
// output: tổng giá trị lũy thừa
function handleClick1(){
    const result=document.getElementById("result1")
    const coso=document.getElementById("coso").value*1
    const somu=document.getElementById("somu").value*1
    let sum=0
    let i=0
    while(i<somu){
        ++i;
        sum=sum+Math.pow(coso,i)
    }
    result.innerHTML=sum
}

// bai 3:Nhập vào n. Tính giai thừa 1*2*...n
// input: số n;
// action: 
// thực hiện vòng lập khi i nhỏ hơn giá trị n; trong vòng lặp sẽ thực hiện phép tính nhân các giá trị i
// output: giai thừa
function handleClick2(){
    const result=document.getElementById("result2")
    const number=document.getElementById("number").value*1
   
    let tich=1
    let i=0
    while(i<number){
        ++i;
        tich=tich*i
    }
    result.innerHTML=tich
}

// bai 4:Nếu div nào vị trí chẵn thì background màu đỏ và lẻ thì background màu xanh.
// input:
// action:
// thực hiện vòng lặp qua các thẻ chẵn để gán thuộc tính backgroundcolor  màu đỏ
// thực hiện vòng lặp qua các thẻ lẻ để gán thuộc tính backgroundcolor  màu xanh
// output: 10 thẻ div có xanh đỏ xen kẽ
const ele=document.querySelectorAll(".ele")

console.log(ele)
const numberEle=ele.length
for(let a=0;a<numberEle;a+=2){
    // console.log(ele[a])
    ele[a].style.width="200px"
    ele[a].style.height="20px"
    ele[a].style.backgroundColor="blue"
}
for(let b=1;b<numberEle;b+=2){
    // console.log(ele[b])
    ele[b].style.width="200px"
    ele[b].style.height="20px"
    ele[b].style.backgroundColor="red"
}
const active=document.querySelector(".active")
      active.style.display="none"
function handleClick3(){
    
    if(active.style.display==="block"){
        active.style.display="none"
    }else{active.style.display="block"}
}